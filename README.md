Two npm-scripts to automatically clone a git repository into a subfolder and subsequently add it to project .gitignore 

Usage
===========

	npm run install-blog
	npm run blog
	./bin/blog.sh <origin> <local> 

Configure the repository origin and installation path in package.json config:blog

*Note that config:blog_repo is depecrated*
