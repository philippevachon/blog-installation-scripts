#!/bin/bash

if [ ! -n "$1" ]; then
  printf "Usage: blog <repo> <local> \n repo\tThe https address of the repo you want to clone into the blog folder\n"
  exit 85
fi 

if [ ! -n "$2" ]; then
  printf "Usage: blog <repo> <local> \n repo\tThe https address of the repo you want to clone into the blog folder\n"
  exit 85
fi 

if [ -d "$2" ]; then 
	echo "local folder already exists. Do you want to delete? (Y/n) "
	read delete
	if [ "$delete" != "Y" ]; then
		exit 1
	else
		rm -rf $2
		echo "Removed local folder"
	fi
fi

git clone $1 $2

if ! [ -e .gitignore ]; then
	touch .gitignore
fi

search=`grep "$2" .gitignore`
if [ -z "$search" ]; then
	echo "$2" >> .gitignore
	echo "Added $2 to .gitignore"
fi
