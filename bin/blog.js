#!/usr/bin/env node

// command line arguments are passed to process.argv
var args = process.argv.slice(2);

var repo = process.env.npm_package_config_blog_origin;
var local = process.env.npm_package_config_blog_local;

if (repo && local){

	var spawn = require('child_process').spawn;

	// Child will use parent's stdios
	var git = spawn('git', ['clone', repo, local], { stdio: 'inherit' });	

	git.on("close", function (code){
		if (code === 0){
			//add blog to gitignore
			var fs = require('fs');
			var gitignore = fs.readFileSync('.gitignore', {flag: "a+", encoding: "utf8"});

			// var match = gitignore.match(/^blog/mg);
			var match = gitignore.match(new RegExp("^"+local, "mg"));

			if (match === null){
				fs.appendFile('.gitignore', '\n'+local, function (err) {
					if (err) throw err;
			  		console.log('local folder added to .gitignore');
				});
			}
		}
	});
} else {
	console.log("Please specify a repo to clone from in your package.json config");
}
